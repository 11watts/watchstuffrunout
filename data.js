const products = [
  {
    name: "Blue Buffalo Adult Chicken & Brown Rice - Dry Dog Food",
    product_id: 52616137
  },
  {
    name:
      "Nature's Recipe Grain Free Easy to Digest (Salmon Sweet Potato & Pumpkin Recipe) - Dry Dog Food",
    product_id: 52035216
  },
  {
    name: "Vitamin D Whole Milk - 1gal - Good & Gather™",
    product_id: 13276134
  },
  {
    name: "1% Milk - 1gal - Good & Gather™",
    product_id: 13276131
  },
  {
    name:
      "Organic Cage-Free Fresh Grade A Large Brown Eggs - 12ct - Good & Gather™",
    product_id: 14662560
  },
  {
    name: "Eggland's Best Grade A Extra Large Eggs - 18ct",
    product_id: 14711486
  },
  {
    name: "Whole Kernel Corn - 15.25oz - Market Pantry™",
    product_id: 13324726
  },
  {
    name: "Garbanzo Beans 15.5 oz - Market Pantry™",
    product_id: 13324645
  },
  {
    name: "Low Sodium Black Beans 15 oz - Market Pantry™",
    product_id: 14484506
  },
  {
    name: "Great Northern Beans 15.5 oz - Market Pantry™",
    product_id: 14484449
  },
  {
    name: "Septic + Sewer Safe Toilet Paper- 4 Rolls - Up&Up",
    product_id: 15819046
  },
  {
    name: "Charmin Ultra Soft Toilet Paper - Mega Rolls",
    product_id: 75665851
  },
  {
    name: "Clorox Splash-Less Liquid Bleach - Regular - 55oz",
    product_id: 14711216
  },
  {
    name: "Purified Drinking Water - 24pk/16.9 fl oz Bottles - Good & Gather™",
    product_id: 54407104
  },
  {
    name:
      "Poland Spring Brand 100% Natural Spring Water - 12pk/12 fl oz Bottles",
    product_id: 16608636
  },
  {
    name:
      "PURELL Advanced Hand Sanitizer Naturals with Plant Based Alcohol Pump Bottle - 2 fl oz",
    product_id: 50520361
  },
  {
    name: "Fresh Scent Daily Shower Cleaner - 32oz - Up&Up™",
    product_id: 76078863
  }
];
//15819046 Septic + Sewer Safe Toilet Paper- 4 Rolls - Up&Up™
module.exports = { products };

/// https://api.target.com/fulfillment_aggregator/v1/fiats/13324726?key=eb2551e4accc14f38cc42d32fbc2b2ea&nearby=98221&limit=20&requested_quantity=1&radius=50
