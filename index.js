const axios = require("axios");
const { products } = require("./data");

const fs = require("fs");

const checkInventory = async (product_id, zip, limit, radius, key) => {
  const result = await axios.get(
    `https://api.target.com/fulfillment_aggregator/v1/fiats/${product_id}?key=${key}&nearby=${zip}&limit=${limit}&requested_quantity=1&radius=${radius}`
  );
  return result.data;
};

const getApiKey = async () => {
  const result = await axios.get("https://target.com/");
  const key = result.data.split('x-api-key","value":"')[1].split('"}}},"t')[0];
  console.log(`key: ${key}`);
  return key;
};
const main = async () => {

  try {
    const k = "eb2551e4accc14f38cc42d32fbc2b2ea"; //await getApiKey();
    const now = new Date();
    console.log(now.getTime());
    fs.mkdirSync(`data/${now.getTime()}`, { recursive: true });
    for (let p of products) {
      try {
        const j = (await checkInventory(p.product_id, 90210, 5000, 50000, k))
          .products[0];
        fs.mkdirSync(`data2/${p.product_id}`, { recursive: true });
        fs.writeFile(
          `data2/${p.product_id}/${now.getTime()}.json`,
          JSON.stringify(j),
          "utf8",
          function(err) {
            if (err) {
              console.log(
                "An error occured while writing JSON Object to File."
              );
              return console.log(err);
            }

            console.log("JSON file has been saved.");
          }
        );

        const available0 = j.locations.filter(
          i => i.location_available_to_promise_quantity === 0
        );
        const outOfStock = j.locations.filter(
          i => i.in_store_only.availability_status === "OUT_OF_STOCK"
        );

        const limitedStock = j.locations.filter(
          i => i.in_store_only.availability_status === "LIMITED_STOCK_SEE_STORE"
        );
        console.log(
          `${p.name} is out of stock in ${outOfStock.length}, limited in ${limitedStock.length}, 0 available_to_promise_quantity in ${available0.length} of ${j.locations.length}`
        );
      } catch (error) {
        console.log(error);
      }
    }
  } catch (error) {
    console.error(error);
  }
};
main();

setInterval(main, 1000 * 60 * 30);

module.exports = { checkInventory };
